<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends CI_Controller {

	
	public function index()
	{
		$this->load->model('Customer_model');
		

		$data['customers'] = $this->Customer_model->get_customer();
		$this->load->view('common/header.php');
		$this->load->view('billing/index.php',$data);
		$this->load->view('customers/new_customers.php');
		$this->load->view('common/footer.php');
	}

	public function AddItem()
	{
		$this->load->model('Billing_model');

		if($this->session->has_userdata('bill_id'))
		{
			$data['bill_id'] = $this->session->userdata('bill_id');
		}
		else{
			$data['bill_id'] = uniqid();
		}
		$data['customer_id'] = $this->input->post('customer_name');
		$data['name'] = $this->input->post('product_name');
		$data['pid'] = $this->input->post('product_code');
		$data['qty'] = $this->input->post('product_quantity');
		$data['price'] = $this->input->post('product_total');
		$this->Billing_model->save_item_for_billing($data);
		$this->session->set_userdata('bill_id', $data['bill_id']);
		echo $data['bill_id'];
		
	}

	public function GetProductsView()
	{
		$this->load->model('Billing_model');
		if($this->session->has_userdata('bill_id'))
		{
			$data['invoices'] = $this->Billing_model->GetProductsView($this->session->userdata('bill_id'));
			$this->load->view("billing/view",$data);
		}
	}

	public function DiscardInovice()
	{
		
		$loc = base_url()."index.php/Billing";
		header("Location:".$loc);
	}

	public function Invoice()
	{
		$this->load->model('Billing_model');
		$data['invoices'] = $this->Billing_model->GetProductsView($this->session->userdata('bill_id'));
		//Store To DB
		$bill['bill_id'] = $this->session->userdata('bill_id');
		$bill['total'] = 0;
		$bill['customer_id'] = 0;
		foreach ($data['invoices'] as $item) {
			$bill['customer_id'] = $item['customer_id'];
			$bill['total'] = $item['price'] + $bill['total'];
		}

		$bill['total'] = $bill['total'] - $this->session->userdata('discount');
		$bill['discount'] = $this->session->userdata('discount');

		$bill['payment'] = $this->session->userdata('payment');

		$this->Billing_model->save_bill($bill);

		$this->Billing_model->save_credit($bill['total']-$bill['payment'],$bill['customer_id']);

		if($this->session->has_userdata('bill_id'))
		{
			$data['invoices'] = $this->Billing_model->GetProductsView($this->session->userdata('bill_id'));
			$this->load->view("billing/bill",$data);
		}

		
		$this->session->sess_destroy();
	}

	public function AddDiscount()
	{
		$this->session->set_userdata('discount',$this->input->post('discount') );
	}

	public function Addpayment()
	{
		$this->session->set_userdata('payment',$this->input->post('payment') );
	}
}
