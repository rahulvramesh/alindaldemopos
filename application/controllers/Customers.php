<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {

	public function __construct()
    {
           // Call the CI_Model constructor
           parent::__construct();
           $this->load->model('Customer_model');
            $this->load->model('Customer_model','customers');
    }
	public function index()
	{
		
		$this->load->view('common/header.php');
		$this->load->view('customers/index.php');
		$this->load->view('common/footer.php');
	}

	public function SaveItem()
	{
		$data['name'] = $this->input->post('customer_name');
		$data['phone'] = $this->input->post('customer_phone');
		$data['address'] = $this->input->post('customer_address');
		$data['credit']  = 0;

		$this->Customer_model->add_customer($data);

		echo true;

	}

	public function ajax_list()
    {
        $list = $this->customers->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->name;
            $row[] = $customers->phone;
            $row[] = $customers->address;
            $row[] = $customers->credit;
            $row[] = $customers->created_at;
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->customers->count_all(),
                        "recordsFiltered" => $this->customers->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
 
    public function ajax_edit($id)
    {
        $data = $this->customers->get_by_id($id);
        echo json_encode($data);
    }
 
    public function ajax_add()
    {
        $data = array(
                'firstName' => $this->input->post('firstName'),
                'lastName' => $this->input->post('lastName'),
                'gender' => $this->input->post('gender'),
                'address' => $this->input->post('address'),
                'dob' => $this->input->post('dob'),
            );
        $insert = $this->customers->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_update()
    {
        $data = array(
                'firstName' => $this->input->post('firstName'),
                'lastName' => $this->input->post('lastName'),
                'gender' => $this->input->post('gender'),
                'address' => $this->input->post('address'),
                'dob' => $this->input->post('dob'),
            );
        $this->customers->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_delete($id)
    {
        $this->customers->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }

	public function ListCustomers()
	{
		$this->load->view("customers/list");
	}
}
