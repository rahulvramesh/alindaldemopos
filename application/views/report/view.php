<div class="ui main container">
<label>Total Sales : <b><?php echo count($entries); ?></b></label>
  <table class="ui celled table">
    <thead>
      <tr>
        <th>Name</th>
        <th>Phone</th>
        <th>Discount</th>
        <th>Total</th>
        <th>Payment</th>

      </tr>
    </thead>
    <tbody>
    <?php if(count($entries) <= 0)
          {
            echo " <tr> <td>No Record Found</td></tr>";
          }
          else{
          ?>
    {entries}

      <tr>
        <td>{name}</td>
        <td>{phone}</td>
        <td>{discount}</td>
        <td>{total}</td>
        <td>{payment}</td>
      </tr>

     {/entries}

     <?php } ?>

    </tbody>
  </table>

</div>