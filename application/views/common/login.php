<!DOCTYPE html>
<html>
<head>
	<title>Alindal POS</title>
 <script type="text/javascript" src="<?php echo base_url()."theme/jquery-3.1.0.min.js"; ?>"></script>
 <script type="text/javascript" src="<?php echo base_url()."theme/semantic.js"; ?>"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url()."theme/semantic.css"; ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()."theme/theme.css"; ?>">
  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>

</head>
 <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            email: {
              identifier  : 'email',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your e-mail'
                }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your password'
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>

  <div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      <img src="http://alindaltechnologies.com/img/logo-dark-2.png" class="image">
      <div class="content">
        Log-in to your account
      </div>
    </h2>
    <form class="ui large form" method="post" action="<?php echo base_url()."index.php/Welcome/login";?>">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="email" placeholder="Username">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Password">
          </div>
        </div>
        <div class="ui fluid large teal submit button">Login</div>
      </div>

      <div class="ui error message"></div>

    </form>

    <div class="ui message">
     Powered By Alindal Technologies
    </div>
  </div>
</div>