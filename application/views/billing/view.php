  <table class="ui basic right aligned table visible">
	      <thead>
	        <tr><th class="left aligned">Id</th>
	        <th>Product Name</th>
	        <th>Quantity</th>
	        <th>Total</th>
	      </tr></thead>
	      <tbody>

	       <?php
	       $total = 0;
	       ?>

	      <?php $i = 1;  foreach ($invoices as $item ) : ?>
	       <tr>
	      	 <td class="left aligned"><?php echo $i; ?></td>
	          <td><?php echo $item['name']; ?></td>
	          <td><?php echo $item['qty']; ?></td>
	          <td><?php echo $item['price']; ?></td>
	          </tr>
	         <?php 
	         $total = $total+$item['price'];
	        
	          ?>
	      <?php $i++; endforeach; ?>

	      <?php

 if($this->session->has_userdata('discount'))
	         {
	         	$total = $total - $this->session->userdata('discount');
	         }

	      ?>
	         
	        
	        
	      </tbody>
	    </table> 
  <div class="ui form  floated right" style="float:right;">
      <div class=" field">
        <label>Discount : </label>
        <input type="text" id="discount" placeholder="0.00" value="<?php echo $this->session->userdata('discount'); ?>">
        <br/>
        <label>Total : </label>
        <input type="text" placeholder="0.00" value="<?php echo $total; ?>">
<br/>
         <label>Payment : </label>
        <input type="text" id="payment" placeholder="0.00" value="<?php echo $this->session->userdata('payment'); ?>">
      </div>
    </div>
	  

<script type="text/javascript">
	;$(function(){
		$( "#discount" ).keyup(function() {
		 var discount  = $( "#discount" ).val();

		 $.post( base_url+"index.php/Billing/AddDiscount", { discount: discount })
		  .done(function( data ) {
		   $.get( base_url+"index.php/Billing/GetProductsView", function( data ) {
					  $( "#invoice_preview" ).html( data );
					});
		  });
		});

		$( "#payment" ).keyup(function() {
		 var payment  = $( "#payment" ).val();
	

		 $.post( base_url+"index.php/Billing/Addpayment", { payment: payment })
		  .done(function( data ) {
		   $.get( base_url+"index.php/Billing/GetProductsView", function( data ) {
					  $( "#invoice_preview" ).html( data );
					});
		  });
		});
	});
</script>