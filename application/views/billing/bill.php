
<style type="text/css">
  @page {
    size: 2.5in 3in;
     margin: 0mm 0mm 0mm 0mm; 
  }

  @media print
{
    .noprint {
        display:none !important;
        height:0px !important;
    }
    .print{
    page-break-after: avoid;

}
}

.print:last-child {
     page-break-after: auto;
}

.print{
    page-break-after: avoid;

}
  body{
    margin: 0px;
    padding: 0px;
    font-weight: 600;
  }
   BODY,
   TD {
   background-color: #ffffff;
   color: #000000;
   font-family: Arial;
   font-size: 10pt;
   font-weight: 600;
   }
   * {
        margin: 0;
        padding: 0;
      
      
        font-size: 100%;
        vertical-align: baseline;
        background: transparent;
    }
    body
{
  margin: 0mm 0mm 0mm 0mm;
}
</style>
<body>
   <table border="0" style="margin-left:10px;">
      <tr>
         <td>
            <table border="0" width="100%">
               <tr>
                  <td align="center" style="font-size: 17pt">
                     <nobr>
                        <!-- <IMG SRC="insert link to the image" width="200" height="200"> -->
                     </nobr>
                  </td>
               </tr>
               <tr>
                  <td align="center">
                     <nobr>
                        <date>
                        <time>
                     </nobr>
                  </td>
               </tr>
               <tr>
                  <td align="center">
                     <nobr>
                        Aadithya Digital Press
                        <salesperson>
                     </nobr>
                  </td>
               </tr>
               <tr>
                  <td align="center">
                     <nobr>
                        Venjaramoodu, Trivandrum 695607
                        <receiptnumber>
                     </nobr>
                  </td>
               </tr>
                <tr>
                  <td align="center">
                     <nobr>
                        Phone : 0472 2870018<br/>
                         www.aadithyapress.com
                        <receiptnumber>
                     </nobr>
                  </td>
               </tr>
               <tr>
                  <td align="center">
                     <nobr>

                        <eventname>
                     </nobr>
                  </td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
               </tr>
            </table>
            <table border="2" width="100%">
             <tr>
                  <td align="left">
                  Product Name
                  </td>
                  <td>
                  Quantity
                  </td>
                  <td>
                   Amount
                  </td>
                 
                  </tr>
             <?php
               $total = 0;
               ?>
               <?php $i = 1;  foreach ($invoices as $item ) : ?>
               <tr>
                  <td align="left">
                     <nobr><?php echo $item['name']; ?></nobr>
                  </td>
                   <td align="right">
                     <?php echo $item['qty']; ?> 
                   </td>
                  <td align="right">
                     <nobr>
                     <?php echo $item['price']; ?>
                     <nickname>
                     <nobr>
                  </td>
               </tr>
               <?php 
             $total = $total+$item['price'];
             
              ?>
              <?php $i++; endforeach; ?>
              <?php
              if($this->session->has_userdata('discount'))
             {
                $total = $total - $this->session->userdata('discount');
             }

              ?>
               <tr>
                 
               </tr>
               <?php if($this->session->has_userdata('discount')): ?>
               <tr>
                  <td align="left">
                     <nobr>Discount</nobr>
                  </td>
                  <td align="right">
                     <nobr>
                     <?php echo $this->session->userdata('discount'); ?>
                     <total>
                     <nobr>
                  </td>
               </tr>
           <?php endif; ?>
                <tr>
                  <td align="left">
                     <nobr>Total</nobr>
                  </td>
                  <td align="right">
                     <nobr>
                     <?php echo $total; ?>
                     <total>
                     <nobr>
                  </td>
               </tr>
                <?php if($this->session->has_userdata('payment')): ?>
               <tr>
                  <td align="left">
                     <nobr>Payment</nobr>
                  </td>
                  <td align="right">
                     <nobr>
                     <?php echo $this->session->userdata('payment'); ?>
                     <total>
                     <nobr>
                  </td>
               </tr>
           <?php endif; ?>
            </table>
         </td>
      </tr>
   </table>
   * All Amount In Rs.
   <br/>
   <br/>
   <br/>
   <br/>
   <br/>
For Aadithya Digital Press<br/><br/>
--------------------------          
<script type="text/javascript">
    window.print();
</script>   
         