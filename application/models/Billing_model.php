<?php
class Billing_model extends CI_Model {

	 public function __construct()
     {
           // Call the CI_Model constructor
           parent::__construct();
     }

     public function save_item_for_billing($data)
     {
     	$this->db->insert('billing', $data);
     }

     public function GetProductsView($bill_id)
     {
     	$this->db->select('*');
     	$this->db->from('billing');
     	$this->db->where('bill_id', $bill_id);

     	$query = $this->db->get();

     	return $query->result_array();

     }

     public function save_bill($data)
     {
     	$this->db->insert('orders', $data);
     }

     public function save_credit($amount,$id)
     {
     	$sql = "UPDATE  `customers` SET  `credit` =  `credit`+$amount WHERE  `id` =".$id;
     	$this->db->query($sql);
  //    	$this->db->set('credit', 'credit'+$amount);
		// $this->db->where('id', $id);
		// $this->db->update('customers');

		//echo $this->db->get_compiled_update();
     }

    

}